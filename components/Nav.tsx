import styled from "styled-components";
import Link from 'next/link'
import {useState} from "react";
import {devices} from "../styles/sizes";
import {useRouter} from "next/router";

type NavStylesProps = {
    menuVisibility: boolean
}

const NavStyles = styled.nav<NavStylesProps>`
  font-family: 'Barlow Condensed', sans-serif;
  font-size: 1.5rem;
  font-weight: 100;
  letter-spacing: 2.7px;
  position: absolute;
  top: 0;
  right: 0;

  .hamburger-icon {
    position: absolute;
    top: 30px;
    right: 30px;
    width: 30px;
    height: 30px;
    cursor: pointer;
    z-index: 2;
  }
  

  ul {
    height: 100vh;
    width: 60vw;
    margin: 0;
    backdrop-filter: blur(20px);
    z-index: 1;
    list-style: none;
    line-height: 4rem;
    text-transform: uppercase;
    padding-top: 100px;
    display: ${props => props.menuVisibility ? "block" : "none"};
  }
  
  

  @-moz-document url-prefix() {
    ul {
      background-color: hsla(0, 0%, 0%, .90);
      backdrop-filter: blur(30px) saturate(125%);
      border: 1px solid rgba(255, 255, 255, 0.2);
    }
  }

  ul span {
    font-weight: bold;
  }

  ul a {
    text-decoration: none;
    color: var(--white);
  }

  ul a:visited {
    color: var(--white);
  }

  /*****************************
           TABLET STYLES
   ******************************/

  @media ${devices.tablet} {
    .hamburger-icon {
      display: none;
    }

    ul {
      display: flex;
      gap: 20px;
      justify-content: flex-end;
      font-size: 1rem;
      height: auto;
      background: none;
      border: none;
      padding-top: 20px;
      padding-right: 50px;
    }

    ul span {
      display: none;
    }

    li.active{
      border-bottom: 3px solid var(--white);
    }

    li:hover{
      border-bottom: 3px solid var(--menu-hover-gray);
    }

    /*****************************
           DESKTOP STYLES 
    ******************************/
    @media ${devices.desktop} {
      ul {
        backdrop-filter: blur(20px);
        padding-right: 150px;
        font-size: 1.3rem;
      }

      ul span {
        display: inline-block;
      }
    }

  }

`

const Nav = () => {
    const [menuVisibility, setMenuVisibility] = useState(false)
    const router = useRouter();

    return <NavStyles menuVisibility={menuVisibility}>
        <img className="hamburger-icon"
             src={`/assets/shared/icon-${menuVisibility ? "close" : "hamburger"}.svg`}
             onClick={() => setMenuVisibility(!menuVisibility)}/>
        <ul>
            <li className={router.pathname == '/'?"active":""}>
                <Link href="/">
                    <a onClick={() => setMenuVisibility(false)}>
                        <span>{`00`}</span> Home
                    </a>
                </Link>
            </li>
            <li className={router.pathname == '/destination'?"active":""}>
                <Link href="/destination">
                    <a onClick={() => setMenuVisibility(false)}>
                        <span>{`01`}</span> Destination
                    </a>
                </Link>
            </li>
            <li className={router.pathname == '/crew'?"active":""}>
                <Link href="/crew">
                    <a onClick={() => setMenuVisibility(false)}>
                        <span>{`02`}</span> Crew
                    </a>
                </Link>
            </li>
            <li className={router.pathname == '/technology'?"active":""}>
                <Link href="technology/">
                    <a onClick={() => setMenuVisibility(false)}>
                        <span>{`03`}</span> Technology
                    </a>
                </Link>
            </li>
        </ul>
    </NavStyles>
}

export default Nav