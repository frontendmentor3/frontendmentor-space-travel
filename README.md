# Frontend Mentor - Space tourism website solution

This is a solution to the [Space tourism website challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/space-tourism-multipage-website-gRWj1URZ3). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)  
  - [Useful resources](#useful-resources)
- [Author](#author)



## Overview

### The challenge

Users should be able to:

- View the optimal layout for each of the website's pages depending on their device's screen size
- See hover states for all interactive elements on the page
- View each page and be able to toggle between the tabs to see new information

### Screenshot

![](./screenshot.jpg)


### Links

- Solution URL: [Gitlab](https://gitlab.com/frontendmentor3/frontendmentor-space-travel)
- Live Site URL: [Vercel](https://frontendmentor-space-travel.vercel.app/)

## My process
- set up project, next.js with typescript
- Create page template (_app.tsx, components/Page.tsx)
- Create Nav
- Index page (mobile, tablet, desktop)
- crew page
- technology page

### Built with

- Next.js
- Flexbox
- CSS Grid
- Mobile-first workflow
- Typescript
- [Next.js](https://nextjs.org/) - React framework
- [Styled Components](https://styled-components.com/) - For styles


### What I learned

1. create new file in Powershell with `New-Item` e.g. `New-Item tsconfig.json`

2. styled component doesn't work properly on load when using _document
- [Server side rendering Styled-Components with NextJS](https://medium.com/swlh/server-side-rendering-styled-components-with-nextjs-1db1353e915e)
- [Custom Document](https://nextjs.org/docs/advanced-features/custom-document)

3. CSS background image in public folder `background-image: url('/assets/home/background-home-mobile.jpg');  `

4. `Typescript complains Property does not exist on type 'JSX.IntrinsicElements'` - styled components name must start with a Capital letter

5. CSS for firefox only
```css
@-moz-document url-prefix(){
    ul{
      background-color: hsla(0,0%,0%,.90);
      backdrop-filter: blur(30px) saturate(125%);
      border: 1px solid rgba(255,255,255,0.2);
    }
  }
```
6. FOUC
  [What the FOUC is happening: Flash of Unstyled Content](https://dev.to/lyqht/what-the-fouc-is-happening-flash-of-unstyled-content-413j)
  [solution](https://joseph.to/solve-fouc-nextjs-and-gatsbyjs/)


7. Changing img src based on screen size
   [Responsive images](https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images)
   ```html
    <picture>
      <source media="(max-width: 799px)" srcset="elva-480w-close-portrait.jpg">
      <source media="(min-width: 800px)" srcset="elva-800w.jpg">
      <img src="elva-800w.jpg" alt="Chris standing up holding his daughter Elva">
    </picture>
    ```


### Useful resources

- [Font Optimization](https://nextjs.org/docs/basic-features/font-optimization) - Using google font in next.js
- [Static File Serving](https://nextjs.org/docs/basic-features/static-file-serving) - files inside folder `public` can be accessed in jsx/tsx starting from the base URL `/`
- [Server side rendering Styled-Components with NextJS](https://medium.com/swlh/server-side-rendering-styled-components-with-nextjs-1db1353e915e)
- [backdrop-blur in firefox](https://medium.com/web-dev-survey-from-kyoto/glassmorphism-for-firefox-2113dff03747)


## Author

- Github - [cherylli](https://github.com/cherylli)
- Gitlab - [cherylm](https://gitlab.com/cherylm)
- Frontend Mentor - [@cherylli](https://www.frontendmentor.io/profile/cherylli)

