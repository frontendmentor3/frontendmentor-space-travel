
import Nav from "../components/Nav";
import {GlobalStyles} from "../styles/GlobalStyles";
import styled from "styled-components";

const LogoStyles = styled.img`
  position: absolute;
  top: 24px;
  left: 24px;
`

const AppContainerStyles = styled.div`
  max-width: 1800px;
  min-width: 300px;
  margin: auto;
  position: relative;
`


function MyApp({Component, pageProps}) {
    return (
        <AppContainerStyles>
            <GlobalStyles />
            <LogoStyles src='/assets/shared/logo.svg' />
            <Nav/>
            <Component {...pageProps} />
        </AppContainerStyles>


    )
}

export default MyApp
