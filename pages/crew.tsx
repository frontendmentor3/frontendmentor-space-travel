import styled from "styled-components";
import {devices} from "../styles/sizes";
import {useState} from "react";
import {CrewDataType} from "../types/types";
import {crewData} from "../data/crewData";


const CrewContainerStyles = styled.div`
  background-image: url('/assets/crew/background-crew-mobile.jpg');
  background-repeat: no-repeat;
  background-size: cover;
  height: 100vh;
  color: var(--light-purple-text);
  text-align: center;
  padding: 0px 40px;  

  h2 {
    font-family: 'Barlow Condensed', sans-serif;
    color: var(--white);
    text-transform: uppercase;
    letter-spacing: 2.7px;
    font-weight: 200;
    padding-top: 100px;
    margin: 0px;
  }

  h2 > span {
    opacity: 0.25;
    padding-right: 20px;
  }

  img {
    height: 250px;
    padding: 40px
  }

  nav {
    button {
      background: transparent;
      color: var(--white);
      opacity: 20%;
      border: 0px;
      font-size: 1.1rem;
      text-transform: uppercase;
      letter-spacing: 2.3px;
      margin: 0px 7px;
      padding: 0px 0px 30px 0px;
      cursor: pointer;
      
    }

    button.active {
      opacity: 100%;      
    }

    button:hover {
      opacity: 60%;
    }
  }

  .crew-role {
    font-family: 'Bellefair', serif;
    text-transform: uppercase;
    color: var(--white);
    opacity: 50%;
  }

  h1 {
    font-family: 'Bellefair', serif;
    text-transform: uppercase;
    font-size: 1.5rem;
    color: var(--white);
    margin: 20px 0px 10px 0px;
  }

  p {
    margin: 0px;
    line-height: 1.5rem;
  }

  

  /*****************************
           TABLET STYLES
   ******************************/
    
  @media ${devices.tablet} {
    background-image: url('/assets/crew/background-crew-tablet.jpg');
    
    h2 {
      text-align: left;
      padding-top: 120px;
    }
    
    h1{
      font-size: 2.5rem;
    }
    
    .crew-role{
      padding-top: 50px;
    }
    
    .crew-bio{
      padding: 10px 0px 40px 0px;
      max-width: 460px;
      height: 100px;
      margin: auto;
    }
    
    .inner-container-tablet{
      display: flex;
      flex-direction: column-reverse;
      justify-content: space-between;
    }

    .desktop-left{
      display: flex;
      flex-direction: column-reverse;
    }      
    
    img{
      height: 400px;
      padding-bottom: 0px;
    }
  }

  /*****************************
          DESKTOP STYLES
  ******************************/
  background-image: url('/assets/crew/background-crew-desktop.jpg');
  
  @media ${devices.desktop} {
    .inner-container-tablet{
      display: flex;
      flex-direction: row-reverse;
      justify-content: space-between;
      align-items: flex-end;
      padding: 0px 40px;
    }
    .desktop-left{
      text-align: left;
    }      
    
    .crew-bio{
      height: 100px;
    }
    
    img{
      width: auto;
      height: 600px;
    }

  }
`

export default function Crew() {
    function getCrew(name: string): CrewDataType {
        return crewData.filter(d => d.name.toLowerCase() === name.toLowerCase())[0] as CrewDataType;
    }

    function changeCrew(name: string): void {
        try {
            setCrew(getCrew(name))
        } catch (e) {
            throw `Change crew failed - Crew ${name} not found.`
        }
    }


    const [crew, setCrew] = useState<CrewDataType>(getCrew('Douglas Hurley'))
    return (
        <CrewContainerStyles>
            <h2><span>02</span>Meet your Crew</h2>
            <div className='inner-container-tablet'>
                <div className='desktop-right'>
                    <img src={crew?.images.webp} alt={`${crew?.name}-image`}/>
                </div>
                <div className='desktop-left'>
                    <nav>
                        <button className={crew?.name.toLowerCase() === 'douglas hurley' ? "active" : ""}
                                onClick={() => changeCrew('Douglas Hurley')}>&#11044;</button>
                        <button className={crew?.name.toLowerCase() === 'mark shuttleworth' ? "active" : ""}
                                onClick={() => changeCrew('Mark Shuttleworth')}>&#11044;</button>
                        <button className={crew?.name.toLowerCase() === 'victor glover' ? "active" : ""}
                                onClick={() => changeCrew('Victor Glover')}>&#11044;</button>
                        <button className={crew?.name.toLowerCase() === 'anousheh ansari' ? "active" : ""}
                                onClick={() => changeCrew('Anousheh Ansari')}>&#11044;</button>
                    </nav>
                    <div>
                        <p className="crew-role">{crew?.role}</p>
                        <h1>{crew?.name}</h1>
                        <p className="crew-bio">{crew?.bio}</p>
                    </div>
                </div>
            </div>
        </CrewContainerStyles>
    )
}
