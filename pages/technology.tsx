import styled from "styled-components";
import {devices} from "../styles/sizes";
import {useState} from "react";
import {technologyData} from "../data/technologyData";
import {TechnologyDataType} from "../types/types";


const TechnologyContainerStyles = styled.div`
  background-image: url('/assets/technology/background-technology-mobile.jpg');
  background-repeat: no-repeat;
  background-size: cover;
  height: 100vh;
  color: var(--light-purple-text);
  text-align: center;
  padding: 0px 0px;

  h2 {
    font-family: 'Barlow Condensed', sans-serif;
    color: var(--white);
    text-transform: uppercase;
    letter-spacing: 2.7px;
    font-weight: 200;
    padding-top: 100px;
    margin: 0px;
  }

  h2 > span {
    opacity: 0.25;
    padding-right: 20px;
  }

  img {
    width: 100vw;
    padding: 0px;
    margin: 40px 0px;
  }

  nav {
    button {
      font-family: 'Bellefair', serif;
      background: transparent;
      color: var(--white);
      border: 1px solid var(--white-25);
      border-radius: 999px;
      width: 40px;
      height: 40px;
      line-height: 40px;
      text-align: center;
      font-size: 1.1rem;
      text-transform: uppercase;
      letter-spacing: 2.3px;
      margin: 0px 10px;
      padding: 0px 0px 10px 0px;
      cursor: pointer;
    }

    button.active {
      background-color: var(--white);
      color: var(--black);
    }

    button:hover {
      border: 1px solid var(--white);
    }
  }

  h1 {
    font-family: 'Bellefair', serif;
    text-transform: uppercase;
    font-size: 1.5rem;
    color: var(--white);
    margin: 15px 0px 10px 0px;
  }

  p {
    margin: 0px;
    padding: 0px 30px;
    line-height: 1.5rem;
  }

  .technology-title {
    font-family: 'Barlow Condensed', sans-serif;
    text-transform: uppercase;
    letter-spacing: 2.3px;
    font-size: 0.8rem;
    font-weight: 200;
    margin-top: 20px;
  }


  /*****************************
           TABLET STYLES
   ******************************/

  background-image: url('/assets/technology/background-technology-tablet.jpg');
  @media ${devices.tablet} {
    h2 {
      text-align: left;
      padding: 120px 0px 0px 40px;
    }

    .technology-title {
      margin-top: 50px;
    }

    p {
      max-width: 400px;
      margin: auto;
    }
  }

  /*****************************
          DESKTOP STYLES
  ******************************/

  background-image: url('/assets/technology/background-technology-desktop.jpg');
  @media ${devices.desktop} {
    display: flex;
    flex-direction: column;
    padding: 150px 0px 0px 160px;
    justify-content: flex-start;

    h2 {
      padding: 0px 0px 40px 0px;
    }

    .desktop-inner-container {
      display: flex;
      justify-content: space-between;
      align-items: center;
      flex-direction: row-reverse;
    }

    .desktop-left {
      margin-top: 80px;
      height: 400px;
      display: flex;
      gap: 40px;
    }

    .technology-title {
      margin: 0px
    }

    .desktop-left-text {
      text-align: left;
      margin-right: 40px;
    }

    h1 {
      font-size: 3.5rem;
    }

    p {
      padding: 0px;
      margin: 0px;
      font-size: 1.2rem;
      max-width: 500px;
    }

    nav {
      display: flex;
      gap: 40px;
      justify-content: flex-start;
      flex-direction: column;

      button {
        font-size: 1.5rem;
        width: 60px;
        height: 60px;
        line-height: 60px;
      }
    }

    img {
      height: 500px;
      width: auto;
    }
  }
`

export default function Technology() {
    function getTechnology(name: string): TechnologyDataType {
        return technologyData.filter(d => d.name.toLowerCase() === name.toLowerCase())[0] as TechnologyDataType;
    }

    function changeTechnology(name: string): void {
        try {
            setTechnology(getTechnology(name))
        } catch (e) {
            throw `Change technology failed - Technology ${name} not found.`
        }
    }

    const [technology, setTechnology] = useState<TechnologyDataType>(getTechnology('Launch vehicle'))
    return (

        <TechnologyContainerStyles>
            <h2><span>03</span>Space Launch 101</h2>
            <div className="desktop-inner-container">
                <div className='desktop-right'>
                    <picture>
                        <source media={devices.desktop} srcSet={technology?.images.portrait}/>
                        <img src={technology?.images.landscape} alt={`${technology?.name}-image`}/>
                    </picture>
                </div>
                <div className='desktop-left'>
                    <nav>
                        <button
                            className={technology?.name.toLowerCase() === 'launch vehicle'.toLowerCase() ? "active" : ""}
                            onClick={() => changeTechnology('Launch vehicle')}>1
                        </button>
                        <button className={technology?.name.toLowerCase() === 'spaceport'.toLowerCase() ? "active" : ""}
                                onClick={() => changeTechnology('Spaceport')}>2
                        </button>
                        <button
                            className={technology?.name.toLowerCase() === 'Space capsule'.toLowerCase() ? "active" : ""}
                            onClick={() => changeTechnology('Space capsule')}>3
                        </button>
                    </nav>
                    <div className='desktop-left-text'>
                        <div className='technology-title'>The Terminology...</div>
                        <h1>{technology?.name}</h1>
                        <p>{technology?.description}</p>
                    </div>
                </div>
            </div>
        </TechnologyContainerStyles>
    )
}
