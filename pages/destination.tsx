import styled from "styled-components";
import {devices} from "../styles/sizes";
import {useState} from "react";
import {destinationData} from "../data/destinationData";
import {DestinationDataType} from "../types/types";


const DestinationContainerStyles = styled.div`
  background-image: url('/assets/destination/background-destination-mobile.jpg');
  background-repeat: no-repeat;
  background-size: cover;
  height: 100vh;
  color: var(--light-purple-text);
  text-align: center;
  padding: 0px 40px;

  h2 {
    font-family: 'Barlow Condensed', sans-serif;
    color: var(--white);
    text-transform: uppercase;
    letter-spacing: 2.7px;
    font-weight: 200;
    padding-top: 100px;
    margin: 0px;
  }

  h2 > span {
    opacity: 0.25;
    padding-right: 20px;
  }

  img {
    width: 180px;
    padding: 20px
  }

  nav {
    button {
      background: transparent;
      color: var(--light-purple-text);
      border: 0px;
      font-size: 0.9rem;
      text-transform: uppercase;
      letter-spacing: 2.3px;
      margin: 0px 10px;
      padding: 0px 0px 10px 0px;
      cursor: pointer;
    }

    button.active {
      border-bottom: 1px solid var(--white);
    }

    button:hover {
      border-bottom: 1px solid var(--menu-hover-gray);
    }
  }

  h1 {
    font-family: 'Bellefair', serif;
    text-transform: uppercase;
    font-size: 3.5rem;
    color: var(--white);
    margin: 20px 0px 10px 0px;
  }

  p {
    margin: 0px;
    line-height: 1.5rem;
  }

  .dest-details {
    margin-top: 50px;
  }

  .dest-details-title {
    font-family: 'Barlow Condensed', sans-serif;
    text-transform: uppercase;
    letter-spacing: 2.3px;
    font-size: 0.8rem;
    font-weight: 200;
    margin: 10px
  }

  .dest-details-content {
    font-family: 'Bellefair', serif;
    text-transform: uppercase;
    font-size: 2rem;
    color: var(--white);
    margin: 10px 0px 10px 0px;
  }

  /*****************************
           TABLET STYLES
   ******************************/

  background-image: url('/assets/destination/background-destination-tablet.jpg');
  @media ${devices.tablet} {
    h2 {
      text-align: left;
      padding-top: 120px;
    }

    .dest-details {
      display: flex;
      gap: 100px;
      justify-content: center;
    }
  }

  /*****************************
          DESKTOP STYLES
  ******************************/
  background-image: url('/assets/destination/background-destination-desktop.jpg');
  @media ${devices.desktop} {
    display: flex;
    gap: 150px;
    padding-top: 200px;
    justify-content: center;

    h2 {
      padding: 0px 0px 40px 0px;
    }

    .desktop-left {
      align-self: flex-start;
    }

    .desktop-right {
      padding-top: 50px;
      max-width: 400px;
      text-align: left;
    }

    img {
      width: 300px;
    }
  }
`

export default function Destination() {
    function getDestination(name: string): DestinationDataType {
        return destinationData.filter(d => d.name.toLowerCase() === name.toLowerCase())[0] as DestinationDataType;
    }

    function changeDestination(name: string): void {
        try {
            setDestination(getDestination(name))
        } catch (e) {
            throw `Change destination failed - Destination ${name} not found.`
        }
    }

    const [destination, setDestination] = useState<DestinationDataType>(getDestination('moon'))
    return (

        <DestinationContainerStyles>
            <div className='desktop-left'>
                <h2><span>01</span>Pick your destination</h2>
                <img src={destination?.images.webp} alt={`${destination?.name}-image`}/>
            </div>
            <div className='desktop-right'>
                <nav>
                    <button className={destination?.name.toLowerCase() === 'moon' ? "active" : ""}
                            onClick={() => changeDestination('moon')}>moon
                    </button>
                    <button className={destination?.name.toLowerCase() === 'mars' ? "active" : ""}
                            onClick={() => changeDestination('mars')}>mars
                    </button>
                    <button className={destination?.name.toLowerCase() === 'europa' ? "active" : ""}
                            onClick={() => changeDestination('europa')}>europa
                    </button>
                    <button className={destination?.name.toLowerCase() === 'titan' ? "active" : ""}
                            onClick={() => changeDestination('titan')}>titan
                    </button>
                </nav>
                <h1>{destination?.name}</h1>
                <p>{destination?.description}</p>
                <div className='dest-details'>
                    <div>
                        <div className='dest-details-title'>Avg. Distance</div>
                        <div className='dest-details-content'>{destination?.distance}</div>
                    </div>
                    <div>
                        <div className='dest-details-title'>Est. Travel Time</div>
                        <div className='dest-details-content'>{destination?.travel}</div>
                    </div>
                </div>
            </div>
        </DestinationContainerStyles>
    )
}
