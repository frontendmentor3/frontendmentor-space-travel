import styled from "styled-components";
import {devices} from "../styles/sizes";
import Link from "next/link";


const IndexContainterStyles = styled.div`
  background-image: url('/assets/home/background-home-mobile.jpg');
  background-repeat: no-repeat;
  background-size: cover;    
  height: 100vh;
  color: var(--light-purple-text);
  text-align: center;
  
  h2{
    font-family: 'Barlow Condensed', sans-serif;
    text-transform: uppercase;
    letter-spacing: 2.7px;
    font-weight: 200;
    padding-top: 150px;
    margin: 0px;
  }
  
  .index-title{
    font-family: 'Bellefair', serif;
    text-transform: uppercase;
    font-size: 6.5rem;
    color: var(--white)
  }
  
  p{
    font-family: 'Barlow', sans-serif;
    padding: 25px;
    line-height: 2rem;
    font-size: 1.1rem;
    font-weight: 300;
  }
  
  .explore{
    font-family: 'Bellefair', serif;
    text-transform: uppercase;    
    font-size: 1.5rem;
    background-color: var(--white);
    color: var(--black);
    width: 180px;
    height: 180px;    
    line-height: 180px;
    border-radius: 9999px;
    margin: 70px auto;
    cursor: pointer;
    
    a{
      text-decoration: none;
      color: var(--black);
    }
    
    a:visited{
      color: var(--black);
    }
  }

  /*****************************
           TABLET STYLES
   ******************************/

  @media ${devices.tablet}{
    background-image: url('/assets/home/background-home-tablet.jpg');
  }

  /*****************************
          DESKTOP STYLES
  ******************************/

  @media ${devices.desktop}{
    background-image: url('/assets/home/background-home-desktop.jpg');
    display: flex;
    align-items: center;
    justify-content: space-around;
    gap: 50px;
    text-align: left;      
    padding-left: 50px;
    
    div:first-child{
      max-width: 450px;
    }
    
    .explore{
      align-self: flex-end;
      margin: 0px 0px 220px 0px;
      text-align: center;      
    }
    
    p{
      padding-left: 0px;
    }
  }
  
`

export default function Home() {
    return (

        <IndexContainterStyles>
            <div>
                <h2>So, you want to travel to</h2>
                <div className="index-title">Space</div>
                <p>
                    Let’s face it; if you want to go to space, you might as well genuinely go to
                    outer space and not hover kind of on the edge of it. Well sit back, and relax
                    because we’ll give you a truly out of this world experience!
                </p>
            </div>
            <div className='explore'>
                <Link href="/destination">
                    <a>Explore</a>
                </Link>
            </div>
        </IndexContainterStyles>


    )
}
