export const fontSize = {
    primary: '16px'
}

const SCREEN_SIZES = {
    tablet: '450px',
    desktop: '1000px'
}

export const devices = {
    tablet: `(min-width:${SCREEN_SIZES.tablet})`,
    desktop: `(min-width:${SCREEN_SIZES.desktop})`
}