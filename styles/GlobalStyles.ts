import {createGlobalStyle} from "styled-components";
import {devices} from "./sizes";

export const GlobalStyles = createGlobalStyle`
  @font-face {
    font-family: 'Barlow', sans-serif;
    //font-family: 'Barlow Condensed', sans-serif;
    //font-family: 'Bellefair', serif;
    font-weight: normal;
    font-style: normal;
  }   
  
  html{
    
    --black: #000000;
    --white: #FFFFFF;
    --white-25: rgba(255,255,255,0.25);
    --light-purple-text: #D0D6F9;
    --menu-hover-gray: #979797;
  }
  
  body{
    font-family: 'Barlow', sans-serif;
    font-size: 16px;
    background-color: var(--black);
    color: var(--white);
    margin:0px;
    padding:0px;      
  }
`