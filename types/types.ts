export type DestinationDataType = {
    name:string,
    images: {
        png: string,
        webp: string,
    }
    description: string,
    distance: string,
    travel: string,
}

export type CrewDataType = {
    name:string,
    images: {
        png: string,
        webp: string,
    },
    role: string,
    bio: string
}

export type TechnologyDataType = {
    name:string,
    images: {
        portrait: string,
        landscape: string,
    },
    description: string,
}